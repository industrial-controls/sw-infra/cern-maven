ARG FROM
FROM $FROM

ARG MAVEN_URL

RUN dnf makecache && dnf install -y epel-release && \
    dnf install -y unzip make binutils rpm-build gcc gcc-c++ rsync git sshpass && \
    dnf clean all

RUN curl -L -o /tmp/mvn.zip $MAVEN_URL && \
    mkdir /opt/maven && \
    mkdir /build && \
    chmod 777 /build && \
    unzip -d /opt/maven /tmp/mvn.zip  && \
    f=(/opt/maven/*) && mv /opt/maven/*/* /opt/maven && rmdir "${f[@]}" && \
    rm /tmp/mvn.zip

# Prepare a relaxed security configuration that permits TLS v1
# for legacy CERN services (like EDMS)
RUN cp /usr/lib/jvm/java-11/conf/security/java.security /opt/java-TLSv1.security && \
    sed -i 's/jdk.tls.disabledAlgorithms=SSLv3, TLSv1, TLSv1.1,/jdk.tls.disabledAlgorithms=/' /opt/java-TLSv1.security

ENV MVN_HOME=/opt/maven
# Works for JDK and JRE installations
ENV PATH $PATH:$JAVA_HOME/jre/bin:$JAVA_HOME/bin:$MVN_HOME/bin

RUN adduser maven

USER root

WORKDIR /build
ENTRYPOINT ["/opt/maven/bin/mvn"]
